import { useSelector, useDispatch } from "react-redux";
import { decrement, increment, incrementN } from "../store/actions";

const INCREMENT_N = 5;
const Counter = () => {
    const counter = useSelector(state => state.counter);
    const dispatch = useDispatch();

    const incrementHandler = () => {
        dispatch(increment())
    }

    const decrementHandler = () => {
        dispatch(decrement());
    }
 
    const incrementNHandler = () => {
        dispatch(incrementN(INCREMENT_N))
    }

    return (
        <div>
            <p>Counter value: {counter}</p>
            <button onClick={incrementHandler}>Increment</button>
            <button onClick={decrementHandler}>Decrement</button>
            <button onClick={incrementNHandler}>Increment {INCREMENT_N}</button>
        </div>
    )
}

export default Counter;